import request from 'supertest';
import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import { app } from '../app';

declare global {
    var signin: () => Promise<string[]>;
}

let mongo: MongoMemoryServer;
beforeAll(async () => {
    jest.setTimeout(6000);
    process.env.JWT_KEY = 'key_secret';
    mongo = await MongoMemoryServer.create();
    const mongoUri = mongo.getUri();

    await mongoose.connect(mongoUri)
});

beforeEach(async () => {
    const collections = await mongoose.connection.db.collections();
    for(let collection of collections){
        await collection.deleteMany({});
    }
});

afterAll(async () => {
    await mongo.stop();
    await mongoose.connection.close();
})

global.signin = async () => {
    const email = 'test@email.com';
    const password = 'password';

    const response = await request(app)
        .post('/api/users/signup')
        .send({email, password})
        .expect(201)

    const cookie = response.get('Set-Cookie');

    return cookie;
}