import request from 'supertest';
import { app } from '../../app';
const endpoint = '/api/users/signin'

it('fails when a email that does not exists is supplied', async () => {
    return request(app)
    .post(endpoint)
    .send({
        email: 'test@test.com',
        password: 'password'
    })
    .expect(400);
});

it('fails when an incorrecto password is supplied', async () => {
    await request(app)
    .post('/api/users/signup')
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(201);
    
    await request(app)
    .post(endpoint)
    .send({
        email: 'test@email.com',
        password: 'BadPassword'
    })
    .expect(400);
});

it('responds with a cookie when given valid credentials', async () => {
    await request(app)
    .post('/api/users/signup')
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(201);
    
    const response = await request(app)
    .post(endpoint)
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(200);
    expect(response.get('Set-Cookie')).toBeDefined();
});