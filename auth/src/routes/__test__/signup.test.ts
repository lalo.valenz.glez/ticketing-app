import request from 'supertest';import { app } from '../../app';
const endpoint = '/api/users/signup'
it('returns a 201 on successful signup', async () => {
    return request(app)
    .post(endpoint)
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(201);
});

it('returns a 400 with an invalid email', async () => {
    return request(app)
    .post(endpoint)
    .send({
        email: 'testasdasd',
        password: 'password'
    })
    .expect(400);
});

it('returns a 400 with an invalid password', async () => {
    return request(app)
    .post(endpoint)
    .send({
        email: 'test@test.com',
        password: 'p'
    })
    .expect(400);
});

it('returns a 400 with missing password and email', async () => {
    return request(app)
    .post(endpoint)
    .send({})
    .expect(400);
});

it('disallows diplicate emails', async () => {
    await request(app)
    .post(endpoint)
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(201);

    await request(app)
    .post(endpoint)
    .send({
        email: 'test@email.com',
        password: 'password'
    })
    .expect(400);
});

it('sets a cookie after succesful signup', async () => {
    const response = await request(app)
        .post(endpoint)
        .send({
            email: 'test@email.com',
            password: 'password'
        })
        .expect(201);
    expect(response.get('Set-Cookie')).toBeDefined();
});

it('returns an user after succesful signup', async () => {
    const response = await request(app)
        .post(endpoint)
        .send({
            email: 'test@email.com',
            password: 'password'
        })
        .expect(201);
    expect(response.body.email).toBeDefined();
    expect(response.body.id).toBeDefined();
});