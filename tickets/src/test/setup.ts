import { MongoMemoryServer } from 'mongodb-memory-server';
import mongoose from 'mongoose';
import jwt from 'jsonwebtoken';

declare global {
    var signin: () => Promise<string[]>;
}

let mongo: MongoMemoryServer;
beforeAll(async () => {
    jest.setTimeout(6000)
    process.env.JWT_KEY = 'key_secret';
    mongo = await MongoMemoryServer.create();
    const mongoUri = mongo.getUri();

    await mongoose.connect(mongoUri)
});

beforeEach(async () => {
    const collections = await mongoose.connection.db.collections();
    for(let collection of collections){
        await collection.deleteMany({});
    }
});

afterAll(async () => {
    await mongo?.stop();
    await mongoose.connection.close();
})

global.signin = async () => {
    const email = 'test@email.com';
    const password = 'password';
    const userId = new mongoose.Types.ObjectId().toHexString();
    const payload = {
        id: userId,
        email
    };
    const token = jwt.sign(payload, process.env.JWT_KEY!);
    const session = {jwt: token};
    const sessionJSON = JSON.stringify(session);
    const base64 = Buffer.from(sessionJSON).toString('base64');
    return [`session=${base64}`];
}