import mongoose from 'mongoose';
import { app } from './app';
import { natsWrapper } from './nats-warpper';


const start = async () => {
    if(!process.env.JWT_KEY){
        throw new Error("JWT_KEY must be defined");
    }
    if(!process.env.MONGI_URI){
        throw new Error("MONGI_URI must be defined");
    }
    console.log('stating app');
    try {
        await natsWrapper.connect(
            'ticketapp', 
            'adadkkk',
            'http://ticketapp-nats-srv:4222');
        await mongoose.connect(process.env.MONGI_URI);
        console.log('connected to mongoDb');
    } catch (error) {
        console.error(error);
    }
    app.listen(3000, () => {
        console.log('Listening on port 3000!');
    });
}
start();

