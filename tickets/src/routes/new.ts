import { requiredAuth, validateRequest } from '@mevgtiks/common';
import express, {Request, Response} from 'express';
import { body } from 'express-validator';
import { Ticket } from '../models/ticket.model';

const router = express.Router();

router.post('/api/tickets',
    requiredAuth,
    [
        body('title')
        .trim()
        .notEmpty()
        .isLength({min: 4, max: 20})
        .withMessage("Title must be between 4 and 20 characters."),
        body('price')
        .isFloat({gt: 0})
        .withMessage('Price must be greater than 0')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
    const {title, price} = req.body;
    const ticket = Ticket.build({
        title, price,
        userId: req.currentUser!.id
    });
    await ticket.save();
    res.status(201).send(ticket);
});

export { router as createTicketRouter } 