import request from "supertest";
import { app } from "../../app";
import { Ticket } from "../../models/ticket.model";

it('has a rout*e handler listening to /api/tickets for posts requests', async () => {
    const response = await request(app)
    .post('/api/tickets')
    .send({});
    expect(response.status).not.toEqual(404);
});
it('can only be accessed if the user is signed in', async () => {
    const response = await request(app)
    .post('/api/tickets')
    .send({});

    expect(response.status).toEqual(401);
});
it('return 200-201 if the user is signed in', async () => {
    const cookie = await signin();
    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'valid title', price: 100});

    expect(response.status).not.toEqual(400);
});
it('returns an error if an invalid title is provided', async () => {
    const cookie = await signin();
    const responseOne = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
        title: '',
        price: 10
    });

    expect(responseOne.status).toEqual(400);
    
    const responseTwo = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
        price: 10
    });

    expect(responseTwo.status).toEqual(400);
});
it('returns an error if an invalid price is provided', async () => {
    const cookie = await signin();
    const responseOne = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
        title: 'My tittle',
        price: -10
    });

    expect(responseOne.status).toEqual(400);
    
    const responseTwo = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({
        title: 'my title'
    });

    expect(responseTwo.status).toEqual(400);
});
it('creates a ticket with valid inputs', async () => {
    let tickets = await Ticket.find({});
    expect(tickets.length).toEqual(0);

    const cookie = await signin();
    const response = await request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'valid title', price: 100});

    expect(response.status).toEqual(201);
    tickets = await Ticket.find({});
    expect(tickets.length).toEqual(1);
    expect(tickets[0].price).toEqual(100);
});