import mongoose from "mongoose";
import request from "supertest";
import { app } from "../../app";
import { Ticket } from "../../models/ticket.model";

it('returns a 404 if the provided id dows not exists', async () => {
    const cookie = await signin();
    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookie)
    .send({title: 'wrong', price: 10})
    .expect(404);
});

it('returns a 401 if the user is not sign in', async () => {
    const id = new mongoose.Types.ObjectId().toHexString();
    await request(app)
    .put(`/api/tickets/${id}`)
    .send({title: 'wrong', price: 10})
    .expect(401);
});

it('returns a 401 if the user does not own the ticket', async () => {
    const cookieUser1 = await signin();
    const response = await request(app)
    .post(`/api/tickets`)
    .set('Cookie', cookieUser1)
    .send({title: 'wrong', price: 10})
    .expect(201);
    const id = response.body.id;
    const cookieUser2 = await signin();
    await request(app)
    .put(`/api/tickets/${id}`)
    .set('Cookie', cookieUser2)
    .send({title: 'wrong 1', price: 30})
    .expect(401);
});

it('returns a 400 if the user provides an invalid title or price', async () => {
    const cookie = await signin();
    const response = await request(app)
    .post(`/api/tickets`)
    .set('Cookie', cookie)
    .send({title: 'wrong', price: 10})
    .expect(201);
    
    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({title: '', price: 10})
    .expect(400);
    
    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({title: 'wrong 2', price: -10})
    .expect(400);
});

it('updates the ticket provided valid inputs', async () => {
    const cookie = await signin();
    const newTitle = 'Updated';
    const newPrice = 20;

    const response = await request(app)
    .post(`/api/tickets`)
    .set('Cookie', cookie)
    .send({title: 'wrong', price: 10})
    .expect(201);
    
    await request(app)
    .put(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send({title: newTitle, price: newPrice})
    .expect(200);

    const updatedResponse = await request(app)
    .get(`/api/tickets/${response.body.id}`)
    .set('Cookie', cookie)
    .send()
    .expect(200);
    
    expect(updatedResponse.body.title).toEqual(newTitle);
    expect(updatedResponse.body.price).toEqual(newPrice);
});