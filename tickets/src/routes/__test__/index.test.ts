import mongoose from 'mongoose';
import request from 'supertest';
import { app } from '../../app';

const createTicket = (cookie: string[]) => {
    return request(app)
    .post('/api/tickets')
    .set('Cookie', cookie)
    .send({title: 'valid title', price: 100});
}

it('can fetch a list of tickets', async () => {
    const cookie = await signin();
    await createTicket(cookie);
    await createTicket(cookie);
    await createTicket(cookie);
    const response = await request(app)
    .get('/api/tickets')
    .send()
    .expect(200);

    expect(response.body.length).toEqual(3);
})