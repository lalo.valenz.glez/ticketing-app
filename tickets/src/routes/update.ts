import { requiredAuth, validateRequest, NotAuthorizeError, NotFoundError } from '@mevgtiks/common';
import express, {Request, Response} from 'express';
import { body } from 'express-validator';
import { Ticket } from '../models/ticket.model';

const router = express.Router();

router.put('/api/tickets/:id',
    requiredAuth,
    [
        body('title')
        .trim()
        .notEmpty()
        .isLength({min: 4, max: 20})
        .withMessage("Title must be between 4 and 20 characters."),
        body('price')
        .isFloat({gt: 0})
        .withMessage('Price must be greater than 0')
    ],
    validateRequest,
    async (req: Request, res: Response) => {
        const ticket = await Ticket.findById(req.params.id);
        if(!ticket) throw new NotFoundError();
        
        if(ticket.userId !== req.currentUser!.id){
            throw new NotAuthorizeError();
        }
    const {title, price} = req.body;
    ticket.set({title, price});
    await ticket.save();
    // res.status(201).send(ticket);
    res.sendStatus(200).send(ticket);
});

export { router as updateTicketRouter } 