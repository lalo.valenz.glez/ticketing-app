// import from '@mevgtiks/common';

import { Event, Publisher, Subjects, TicketCreatedEvent } from "@mevgtiks/common";

export class TicketCreatedPublisher extends Publisher<TicketCreatedEvent>{
    subject: Subjects.TicketCreated = Subjects.TicketCreated;
}