import nats, {Message} from 'node-nats-streaming';
import {randomBytes} from 'crypto';
console.clear();

const stan = nats.connect('ticketapp', randomBytes(4).toString('hex'), {
    url: 'http://localhost:4222'
});

stan.on('connect', () => {
    console.log('Listener connected to nats');
    const options = stan.subscriptionOptions()
                        .setManualAckMode(true);

    const subscription = stan.subscribe(
        'ticket:created',
        'orderServiceQueueGroup',
        options
    );
        subscription.on('message', (msg: Message) => {
            const data = msg.getData();
            if(typeof data === 'string'){
                console.log(`Received event #${msg.getSequence()}, with data: ${data}`);
            }
            msg.ack();
        });
});